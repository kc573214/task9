import os

import mlflow
from flask import Flask
from flask import request
from flask import abort

os.environ["MLFLOW_S3_ENDPOINT_URL"] = "http://65.108.80.137:19001"
os.environ["MLFLOW_TRACKING_URL"] = "http://65.108.80.137:5555"
os.environ["AWS_ACCESS_KEY_ID"] = "IAM_ACCESS_KEY"
os.environ["AWS_SECRET_ACCESS_KEY"] = "IAM_SECRET_KEY"

mlflow.set_tracking_uri("http://65.108.80.137:5555")
model = mlflow.pyfunc.load_model("models:/iris_wrapper/Staging")

app = Flask(__name__)


@app.get("/get_source_iris_pred")
def source_pred():
    sepal_length = request.args.get("sepal_length", type=float)
    sepal_width = request.args.get("sepal_width", type=float)

    preds = model.predict([[sepal_length, sepal_width]])["class"]
    return {"prediction": preds[0]}


@app.get("/get_string_iris_pred")
def string_pred():
    sepal_length = request.args.get("sepal_length", type=float)
    sepal_width = request.args.get("sepal_width", type=float)

    preds = model.predict([[sepal_length, sepal_width]])
    preds["class"] = preds["class"][0]
    preds["class_str"] = preds["class_str"][0]
    return preds
