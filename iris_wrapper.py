import mlflow.pyfunc
import numpy as np


class IrisWrapper(mlflow.pyfunc.PythonModel):
    def __init__(self, model, threshold_0_1, threshold_1_2):
        self.model = model
        self.threshold_0_1 = threshold_0_1
        self.threshold_1_2 = threshold_1_2

    def predict(self, context, model_input):
        predictions = self.model.predict(model_input)

        class_names = np.array(["virginica"] * len(predictions), dtype="object")
        class_names[predictions < self.threshold_1_2] = "versicolor"
        class_names[predictions < self.threshold_0_1] = "setosa"

        return {
            "class": predictions,
            "class_str": class_names,
            "threshold_0_1": self.threshold_0_1,
            "threshold_1_2": self.threshold_1_2,
        }
